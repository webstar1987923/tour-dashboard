import { A } from '@ember/array';
import Component from '@ember/component';

export default Component.extend({
	talkTags: A([ {id:0, tag:'Malaga'}, {id:1, tag:'Barcelona'}])
});
