import { A } from '@ember/array';
import Component from '@ember/component';

export default Component.extend({
	guides: A([
		{
			name: "La Maria",
			stars: 4,
			img: "images/guides/maria.png",
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds",
			langs: [ "English", "French", "Russian"],
			rate: 20
		},
		{
			name: "Adam Gilcrist",
			stars: 3.5,
			img: "images/guides/gilcrist.png",
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds",
			langs: [ "English", "French", "Polish"],
			rate: 15
		},
		{
			name: "Matthew Adams",
			stars: 4.5,
			img: "images/guides/adams.png",
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds",
			langs: [ "English", "French", "Korean"],
			rate: 30
		},
		{
			name: "Lara Max",
			stars: 4,
			img: "images/guides/max.png",
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds",
			langs: [ "English", "French", "Russian", "Polish"],
			rate: 22
		}
	])
});
