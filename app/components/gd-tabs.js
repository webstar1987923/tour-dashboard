import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
	didInsertElement: function() {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
			$("#sidebarCollapse").css("display", "none");
		});
	}
});
