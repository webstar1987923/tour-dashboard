import { A } from '@ember/array';
import Component from '@ember/component';

export default Component.extend({
	tours: A([
		{
			name: "Nightclub & Bars",
			stars: 4,
			loc: "City Tour in Spain",
			price: 100,
			hours: 2,
			img: "images/tours/1.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdfdgffgsfdgdsfgdsgfsdgsdioererererererererererererererererererererdfgfdgdgdslkskdslk dsdsldssd dsds"			
		},
		{
			name: "Scuba Diving",
			stars: 3,
			loc: "City Tour in Brazil",
			price: 220,
			hours: 5,
			img: "images/tours/2.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds"
		},
		{
			name: "Local Experience",
			stars: 3,
			loc: "City Tour in Australia",
			price: 110,
			hours: 3.5,
			img: "images/tours/3.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds"
		},
		{
			name: "Nightclub & Bars",
			stars: 3,
			loc: "City Tour in Spain",
			price: 100,
			hours: 2,
			img: "images/tours/4.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds"
		},
		{
			name: "Scuba Diving",
			stars: 4,
			loc: "City Tour in Atlanta",
			price: 220,
			hours: 5,
			img: "images/tours/5.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds"
		},
		{
			name: "Local Experience",
			stars: 3,
			loc: "City Tour in Australia",
			price: 110,
			hours: 3.5,
			img: "images/tours/6.png",			
			desc: "slkdk dskdf dsksdfk dsdjsfsdk dkldklsdk dskdsdskdslk dsdsldssd dsds"
		},
		
	])
});
