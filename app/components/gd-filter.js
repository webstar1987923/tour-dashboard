import { A } from '@ember/array';
import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
	languages: A(['English', 'Spanish', 'French', 'Chinese', 'Japanese', 'Dutch', 'Russian', 'Korean', 'German']),
	selectedLanguages: A([]),

	searchTypes: A(['Private Tours', 'Shore Excurtions', 'Guides']),
	selectedSearchTypes: A([]),

	tourTypes: A(['Any', 'Adventure Tours', 'Biking', 'City Tours', 'Cultural Tours', 'Family', 'First Time Visitor', 'Fishing',
				'Food & Wine', 'Full Day Tours', 'Museums', 'Local Experience', 'Hiking & Walking', 'Nature & Wildlife', 'Religious Heritage',
				'Outdoor Tours', 'Nightclub & Bars', 'Sailing', 'Scuba Diving', 'Shopping', 'Teens', 'Theme Parks', 'Water Sports', 'Winter Sports']),
	selectedTourTypes: A([]),

	appropriateTypes: A(['Kids', 'Handicapped']),
	selectedAppropriateTypes: A([]),

	endurance: 'High',

	mydate: '',

	didInsertElement: function () {
		$('.sidebar-header').on('click', function () {
			$('#sidebar').toggleClass('active');
			$("#sidebarCollapse").css("display", "block");
		});
	}

});
