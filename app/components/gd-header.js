import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
	didInsertElement: function() {

		$(".menu-btn").click(function() {
			$(".menu-btn").toggleClass("active");
			$(".navbar").slideToggle(400);    	    	
		});

		$(window).resize(function(){
			if($(window).width() > 768){
				$('.navbar').removeAttr('style');
				$("#sidebarCollapse").css("display", "none");
			} else {
				$("#sidebarCollapse").css("display", "block");
			}
		});        		        
	}
});
